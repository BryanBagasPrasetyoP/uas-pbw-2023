var daftarPesanan = [];

$(document).ready(function() {
   
    $('#form-pesanan').submit(function(event) {
      event.preventDefault();

    console.log(daftarPesanan);
 
    
    var nomerMeja = $('input[name=nomer_meja]').val();
    var menuPesanan = $('input[name=menu_pesanan]').val();
    var jumlahPesanan = $('input[name=jumlah_pesanan]').val();


    daftarPesanan.push({
      nomerMeja: nomerMeja,
      menuPesanan: menuPesanan,
      jumlahPesanan: jumlahPesanan
    });


      $('#tabel-pesanan').append(
        '<tr>' +
        '<td>' + nomerMeja + '</td>' +
        '<td>' + menuPesanan + '</td>' +
        '<td>' + jumlahPesanan + '</td>' +
        '<td>' +
        '<button class="edit-btn">Edit</button>' +
        '<button class="delete-btn">Delete</button>' +
        '</td>' +
        '</tr>'
      );


      $('input[name=nomer_meja]').val('');
      $('input[name=menu_pesanan]').val('');
      $('input[name=jumlah_pesanan]').val('');
    });
  

    $('#tabel-pesanan').on('click', '.delete-btn', function() {
      // mencari index data di dalam array yang akan dihapus
      var index = daftarPesanan.findIndex(function(item) {
        return item.nomorMeja == $(this).parent().parent().find('td:nth-child(1)').text() &&
              item.menuPesanan == $(this).parent().parent().find('td:nth-child(2)').text() &&
              item.jumlahPesanan == $(this).parent().parent().find('td:nth-child(3)').text();
      });


      daftarPesanan.splice(index, 1);


      $(this).parent().parent().remove();
    });

    

    $('#tabel-pesanan').on('click', '.edit-btn', function() {
      // mencari index data di dalam array yang akan diedit
      var index = daftarPesanan.findIndex(function(item) {
        return item.nomorMeja == $(this).parent().parent().find('td:nth-child(1)').text() &&
              item.menuPesanan == $(this).parent().parent().find('td:nth-child(2)').text() &&
              item.jumlahPesanan == $(this).parent().parent().find('td:nth-child(3)').text();
      });


      var nomorMeja = $('input[name=nomer_meja]').val();
      var menuPesanan = $('input[name=menu_pesanan]').val();
      var jumlahPesanan = $('input[name=jumlah_pesanan]').val();


      daftarPesanan[index].nomerMeja = nomerMeja;
      daftarPesanan[index].menuPesanan = menuPesanan;
      daftarPesanan[index].jumlahPesanan = jumlahPesanan;


      $(this).parent().parent().html(
        '<td>' + nomerMeja + '</td>' +
        '<td>' + menuPesanan + '</td>' +
        '<td>' + jumlahPesanan + '</td>' +
        '<td>' +
        '<button class="edit-btn">Edit</button>' +
        '<button class="delete-btn">Delete</button>' +
        '</td>'
      );


      $('input[name=nomer_meja]').val('');
      $('input[name=menu_pesanan]').val('');
      $('input[name=jumlah_pesanan]').val('');
    });
  });